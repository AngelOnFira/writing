# Veloren - A Historic Background

To see a Timeline of the events referred to in this document, see here: [Veloren - Historical Timeline](https://docs.google.com/drawings/d/1yw0X5HQH8O7JY2XKm3yB3XeKFh4f2brv8slmD--DTcM/edit?usp=sharing)

## History of The Great Malady

More than a millennium ago, The Great Malady befell Veloren. The Dark Master planted his Velvet Black Banner on the shores of Veloren and brought with them a wildly varied and seemingly endless amount of soldiers, magic creatures and slaves. This massive host of deadly creatures and soldiers made up the nomadic troops and people of the Velvet Black Banner. 

The Conquest was in full swing and the fate of elves and dwarves seemed sealed as somehow the Great Mind, the Master behind the Conquest, roared out in a cry of surprise and pain. The shout thundered over the land and all magic users under the Velvet Black Banner convulsed under a sharp pain as their master died. An abhorrent wave of wet, chunky and red explosions, pained shrieks and roares echoed over the front lines, encampments and cities under the invader’s control as most of their casters died.

Any magic user trained under control of The Velvet Black Banner below  a certain talent and magical strength simply died, their heads bursting, unable to withstand the pained deathcry of their master. Most of the Velvet Black Banners magical creatures just fell to the ground, dead on the spot they were standing on. Tens of thousand of cavalry troops, mounted on predominantly magical creatures, went to ground mid run, in a deafening and deadly avalanche of clanging armors and pain. Other creatures, many of immense age and might, only held under control by the Master's call, saw themselves finally freed, and left. Those that were of a more animalistic nature, corrupted or had simply gone insane at some point, fell upon their former allies and handlers to shred them to pieces, and either died doing so or fled. 

Some of those creatures were generals of the army and joined together with other leaders, to voice claims on the leadership over the tribes and cultures under The Velvet Black Banner, even as their army struggled to recover from the death of most of their magic users. They needed a new leader fast, or The Velvet Black Banner would have grave problems. 

Any resources that weren’t plundered from their enemies were gained through magical means, then refined and worked into usable shapes by forced labor and magic. Much of it needed to be conjured by magic because the crusade often left nothing in its wake but ash and plundered, dead lands.

The death of their master had not just killed most of the magic users, the magic that held the Velvet Black Banners slaves subservient had fallen away and slave riots broke out in the rear lines of the army. The anger from generations of abuse now unloaded itself onto slavers and foremen. Unable to unite under a new leader to take care of these problems, it was very clear that the host would soon run out of resources, most importantly food.   

The troops of the Velvet Black Banner, a nomadic army so massive that its rear and resupply lines covered nearly half of Veloren, broke apart. After nearly a century of war against the Dwarven and Elven cultures, after driving them close to extinction, robbing them of their culture and leaving the Veloren continent a ruined dead husk of its former beauty, the Velvet Black Banner broke apart because they were going to run out of food.

The truth of the situation was that with their master’s bond over the troops gone many of the races and factions , corrupt, animalistic or otherwise, simply had only one thing in common: somewhere in the past they had been enemies. There was a week of silence. Rising tensions laid over the former allies as everyone tried to find out who was an ally and who was an enemy. Realizing that most all of them were enemies, absolute chaos broke loose.


The war had confined the Elves to the Pillar cities and forced the ailing Dwarves to a life under the Mountains. For both of these races the following decades of violent chaos and attrition was just a continuation of the invasion, as different factions tried to break the defense of their respective sanctuaries over and over.

Hunger soon engulfed and starved most of what was left of the once mighty Armies of The Velvet Black Banner, and the less  said about the horror of these decades the better.
Few of the troops or enslaved people managed to flee over the oceans, others degraded to small desperate tribes that managed to scratch together a  meager living off the dying continent. It was a living but it wasn’t much.

The results of this Era of strife and struggle are still visible in Veloren and have greatly influenced the cultures of today.    


## The Kin of the Elves

Elves are gifted with the ability to weave magic from birth. How great any individual's power will be depends much on the Elves' health and education. Just as a hungry human can’t run very far, neither  can an Elf suffering from great hunger cast great works of magic.

During The Great Malady that ravaged Veloren lands, Elves and Dwarves came together to build the Pillar Cities. Large Fortresses of seemingly impregnable stone towered over the landscape and helped the Elves defend themselves. Even after the invasion force of The Velvet Black Banner collapsed, the Gold Elves kept themselves closed off from the outside world for many centuries. A great number of the Gold Elves still live in the cores of these grandiose Pillars. Built with magic and the help of experienced dwarven architects, they are still the most important centres of Gold Elven civilisation and power. 

### Gold Elves

live predominantly in these Pillar Cities. The City Kingdoms of the Gold Elves outwards wield much political and economic power and play a great role in the events of Veloren. What most people know of the Gold Elves is their life in the glorious and shining cities on top of the pillars. Though most of them live in the dense and countless halls of the city cores to which only few outsiders have official access. To the outside this makes them appear as a people of great unity, order and strength. A People  that would never yield to anyone. 

But the truth is far from ideal as the Kingdoms are afoul with corruption and greed. The powerful on the top holding their might over their people in the Pillars, making it hard for anyone that doesn’t have enough influence or coin to leave the bowels of the cities. The lower one treads in the city the desperate the circumstances become. The people are strained and political turmoil is a constant. Great speeches to unity, elven power and patriotism ring evermore hollow as the workers toll and live in terrible circumstances. And the narrow tunnels and shady Chambers hall from whispers of revolution.

Most Gold Elves that the outside world gets to see are from influential houses or rich families. With fair, healthy complexion and a slight tan as this is the privilege of the Surface classes. But more and more dwellers of the deeper layers find their way outside. Most of them leave the cities in service of the Elven army that recruits their foot soldiers from the less fortunate populace of the deeper layers of the city. But more of the average citizenry can be seen these days as they are needed to spread Gold Elven power not only in political influence but also to grow Elven reign over the land. Wishing to be less dependent on the food deliveries of other Powers the Kingdoms need to uplift farmers and workers to the surface.


These Gold elves show a much paler skin tone due to generations of living in the tunnels and halls of the Pillar Cities. Most wear wide cramped hats, long sleeved clothing and tinted glasses in woven, wooden frames to protect them from the full shine of the sun. Most don’t dare use that chance to flee the Kingdoms grasp, as they fear being hunted by the order of the Dark Elves or retribution done to their families at home. 
 
### Dark Elves

are a very old order of the Gold Elves Kingdoms. The Order was founded on rules of crisis management, supposed to keep the order within the enclosed cities, during The Great Malady when no living thing could step outside the city walls and live. Over the centuries the order has become a people within the people, as its members prefer to marry within their own membership. This has given them a much darker tone of skin. The Orders members are sworn to protect the Pillar Cities from its enemies and keep them from falling or failing. Lack of Loyalty towards Gold Elve culture is taken gravely serious.  Where Dark Elves walk they awake fears in the Gold Elves populace. Not even the kings are safe should the Orders Leaders deem them unfit and think they might damage the security of the Fortress Cities. But the growing number of the populace is stretching their forces too thin to burst and, just like with Gold Elven Nobility, corruption has spread within the families that are leading the Order.

Dark Elves are seen as relentless in their beliefs and as near zealous fighters for order and honor of the Pillar Cities. They are easy to identify with their nearly black-blueish skin tone and their uniform like clothing.  

The Dark Elves hate the Brushwood Elves for them siding with the humans. After the Gold Elves left the Pillar Cities for the first time and immediately attacked the then still weak Humans since they hadn’t really been some sort of united or allied faction yet.

### Pale Elves
are the mysterious wise of the Golden Elves. An order of seemingly creeping, and extremely secretive Elves, they live deep, deep below the most desolate layers of the cities, in dark and foreign chambers. There they dwell and research ancient knowledge or wander through far off dark halls and caves. For what purpose no one knows or dares to ask. Only few make the perilous journey downwards to the Pale Elves halls to seek their advice, knowledge and blessing, though regular food and supply deliveries are being delivered downwards. No King of the Cities can be crowned without visiting them and seeking their approval. The Pale Elves also hold great sway over the Order of the Dark Elves. From time to time it happens that Elves can be seen walking downwards like in trance. This is seen as a sign that they will join the order. No one would dare to stop these poor blessed souls and doing so can incur the wrath of the Dark Elves. 

Barely anyone sees them nowadays. Especially not Outsiders since as the Pale Elves do not walk under the sun. Being mysterious pale creatures in the dark one would be greatly amiss to underestimate their power and pull over the surface. Many have fruitlessly tried to end their influence and paid dearly for it. 


### Brushwood Elves

are Elves that left the Cities many, many hundred years ago as The Great Malady had long come to an end. The Gold Elves still kept the cities closed as the Land was barren and damp, a desolate place without much life. The ancestors of the Brushwood Elves fled the cramped desolate cities. With them they brought ancient seeds from the vaults of the cities. Escaped from the confines of the cities they started planting and growing plant life. It was these Elves that nurtured nature back to life. It was these elves that heeded the first human nomads welcome in the then barren lands and helped them settle in. Most human cultures have since forgotten much of this but together they worked and toiled hard and raised a new green Veloren. It was also humans and Brushwood elves together that stood against the Orcs when these first appeared on Veloren. Seeking to undo the hard work of humans and elf they tried to plunder the young forests and savage the fresh grown meadows. The Brushwood elves never forgot this and many of them harbor a warm friendly memory and attitude to their old friends, even when those people couldn’t keep the memory. Even when the humans threaten to fight wars against the elves most of the elves effort is to prioritize peace with their Kingdoms.  

In the center of the Brushwood ever reaching forests grows the oldest tree on Veloren. Visible from near every corner of Brushwood land, its presence strengthens the Wood elves magic.Though many have seen it due to its size, only few foreigners have actually had the honor to visit it. Most of whom were humans since the elves still honor the diligence of the human ancestors that helped heal Valoren wounds with the hard work of their hands.

Because of this the Brushwood Elves did not hesitate a moment when the Gold Elves attacked the young human settlements immediately after leaving the Pillar Cities for the first time. Brushwood Elves can be rather hostile to the Nobles of the Gold Elves. Banditry aimed at Gold Elven nobles venturing too close to the Brushwood isn’t seldom. No Gold Elven Army has left the Brushwood Forest as they entered it and the relations are hostile today still.   

There is a story from a few hundred years ago that tells the saga of a brave Orc Chief that slipped through the Brushwood elves guards to steal a seed from this holy tree to give to her tribes. Which is not quite the truth.

## Dwarves - The Great Labor

The Dwarves once had a great and varied culture spanning the colder zones of the world.
Great Dwarven Cities wound along most Mountain regions of Veloren. Dwarves and Elves were close Allies and friends. Exchange of culture and trade between them was stable and complex. The two cultures were woven tightly with one another. So when the darkness rose on the Elven borders the Dwarves did not hesitate and took up warhammer and axe to aid their friends. 

For centuries, Dwarves and Elves fought together in The Great Malady against the armies of The Velvet Black Flag. Together they devised and built the Pillar Cities, mighty Fortresses as the last refuge of both their cultures. Allied army after Allied army, in the South as in the North, stood desperately before the wave of enemy soldiers and corrupted creatures that were burning and ravaging the fruits of the dwarven and elven civilisation, in valleys and plains as well as in the Mountains. Together, architects and magicians erected the Pillars with great effort and under disregard for the personal lives of Thaumaturge and Workers. And as the Pillar Cities neared completion the Elves betrayed the Dwarves.  
  
The great gates of the cities closed off for any and all Dwarven people and they were swept away before the Darkness. Without choice they drew back into their great mines and their workshops in the mountains where they tried to hold out as long as they could. As the Dwarven Culture seemed lost、an unlikely ally came to their rescue. An ally that asked the dwarves to forgive them for their part in the destruction of Veloren. 


Like the Elves the Dwarves pulled back into their sanctuary in the mines and closed their gates for many centuries but opposed to the Elven people the dwarves have not forgotten those that helped them and those that betrayed them.  


While the dwarves stayed inside their mountains to recover their civilization, they still sent out foraging parties to look for wood, seeds and fresh animal stock. And they still sent scouts to keep an eye on the outside world. When these scouts reported that the Gold Elves attacked early human civilisation, the Dwarves opened the massive gates to their subterranean cities. And there was no mistaking their ambitions as they aided the human kingdoms in the war against the Gold Elves. Nowadays most settlements outside their Mountain Cities are massive Workshops, an ocean of smoking chimneys under a sky of black smoke. Day and night, countless hammers work metals and stone into tools, weapons and hammers. Out of the  range of smoke and soot, dwarves till massive fields of crops, vegetables and mushrooms to fill their storage with food and improve the quality and nourishment of their diets. Infinite tunnel systems draw through the mountains to track down even the last chunk of metal in the ground. The Dwarven army is ever growing, the draft being seen as a great honor for every citizen. Dwarves leaving the Army only do so when they know they can be more help in the workshops and industries. But every single Dwarf, even those not part of the army, regularly come to the massive Barracks that are spreading over Dwarven Land to practise drawing down a mighty hammer and pulling a sharp axe. Military discipline is in every facet of dwarven culture. Even workshops using ranks to discern between Adept, Foreman and Master worker. Outwards the army is protecting dwarven lands and fending off mysteries stirred awake from the noise of the dwarves ever growing mines. But this Culture of craftsmen and grim fighters are focused on another grim goal. A goal given to every dwarf, old enough to listen to tales of the grim past and adult enough to keep the silence about the path to the future. Much of Dwarven Culture seeks to gain just payment for their ancestors' work, to remind the Elves of their cowardly betrayal and to take back their share of the Pillar Cities they once helped build.

## Humanity, The Unruly Children of Veloren

Long after the fires of The Great Malady had stopped burning, many of its beastly creatures, and splintered factions roamed the world, some wandering far beyond Velorens shores. And in flight from one horde of such creatures Human settlers first left their Islands. After a perilous journey they finally landed their boats on the shores of Veloren.   
At this time Veloren was a barren Land with only pockets of green growing in hidden crevazes and valleys. They had brought seeds with them but most humans so far had lived from fishing, hunting and gathering of fruits and wild vegetables. Here and there they had smaller fields but not enough agriculture to feed themselves with it. Unable to turn back the humans spread over the land hunting the few edible beasts they could find. The humans became Nomads and travelled a Veloren that was for the most part still void of any Elves or Dwarves since these at that time still were holed up inside their Fortresses.  

However after a few generations on the lonely continent the Nomadic Humans met the Brushwood Elves (which they didn’t call themselves yet since there were no woods yet).
The two groups immediately became close allies and later friends.
The Elves taught the humans animal husbandry and farming. And they told them of The Great Malady that wounded the land and their vision to heal Veloren.
Over the years most of the nomadic humans were drawn to what today is the center of Brushwood. Under the diligent hands of Human and elves the green spread and grew.
Animals, before hidden and spread sparsely far over the empty surface of Valoren, flourished again and carried seeds over the land, spreading the green growth even further. 

Together they also fought off the first contact and clashes with the Orcs tribes, who befell the land and were ravaging the borders of the vulnerable vegetation, avariciously seeking the wood of the still young forests. But this remains just a side note in history, since the roaming Orc tribes at that time were more busy fighting each other then elves and humans.

While the Elves stayed with their growing Brushwood Forest, soon taking on the name they gave it for themselves, the humans spread out thinly over the continent. With humans establishing farms and forresteries plantlife grew back fast, all over Veloren. And thanks to the nutritious ash and seeds spread by animals it got a stable hold.  For a long time humanity could grow undisturbed and comfortably. Many regions were and are still thinly settled. From the Brushwood forest to the coast where humanity had first stepped on Veloren, a vulnerable band of civilisation, small Kingdoms and tribes, had been established. There was trade and little exchange of culture. But the connections were loose at best and everyone kind of did their own thing. And for a time Veloren was at peace..

Then the Gold Elves opened up their cities and sent their armies into the world outside of their Spire Cities. Assuming everything outside to be their enemies, they attacked human settlements and drove them from their homes and fields. Pushing human civilization along its thin line of settlements caused the many Kingdoms and Tribes to unite. Together with the Brushwood Elves and experienced from the conflicts with the Orcs they stopped the surge of the Gold Elves. The war threatened to become a longborn and dirty stalemate as armies of Dwarves marched down from the mountains. Roused from their Underground Cities by the sound of fighting the dwarves did not hesitate to fight against the Gold Elves with great fervor. 

After the war ended with a truce the rulers of the once sleepy domains of humanity recognised the signs of the time and banded together. Then some of the split up and fought each.... Oh no wait they have peace agai… oh... what? Now the south fights the north and… ah peace again! No wait… ah whatever. Safe to say humanity started spreading their political and cultural influence, fought many wars small and great and learned enough to be a challenge to the reemerging Gold Elves, the Dwarves or the Orcs. This started the Age Of Competition.


Not having suffered such a historic genetic blood loss to their people (and then being trapped for centuries in relatively close confines if fortresses) as it has happened to Elves and Dwarves humans are extremely diverse in stature, tone of skin and all the other markers that make a person unique. And humans also have no qualms to seek their live partners within the other races of Veloren what makes the whole populace just even more colorful.

## The Orc Warbands

Not even the oldest Shamans know where the Orcs once came from. But they know how the Orcs arrived on Veloren. They arrived on Velorens shores as one of the many subjugated races under the Velvet Black Banner.  

The Armies of the Velvet Black banner only considered magic users of a certain talent as worthwhile and worth training. Either they were able to great and powerful magic or they weren’t trained at all. The wildly different races and creatures of the invaders were held together as allies by a magic that dimmed most magic users mind. So many of the more powerful casters had problems to stay rational and off a clear mind of their own. Instead fanaticism to their dark leader and to their cause of conquest was a prevalent mindset. But fanatics with a blinded mind make bad soldiers in pressing and critical situations. 

This did not affect most of the Orcs. Orcs had either a strong connection to magic or none at all. Though Orcish shamans were under its influence, the great majority of the Orcs was not affected by the control magic. It was this ability of the Orcs, to keep a clear mind of their own, which saved their existence as a species. One of the Generals offered the Orcs their lives for their service as he saw great problems in the blind fanatic rage and the mindlessness that was inherent to many of the Velvet Black Banners troops. In the conquest of Veloren this mindless rage cost the invaders many victories as Elves and Dwarves knew how to fight and defend their cities with great skill. What was usually a matter of years became a matter of decades. And so the Orcs were dragged from one burning frontline to the next, dying while trying to make up for the incompetence of other generals.


The tribes of the Orcs were part of a siege on a Dwarven mine when the Master of the Velvet Black Banner died. The deathcry of the Master killed most of the orc shamans instantly or drove them to madness. As the unity of the Velvet Black Banner fell apart and into chaos and open conflict the Orcs rallied all of their tribes. 

Not knowing where to go in the chaos, they went to the only faction that showed some form of order and sanity. They went to the dwarves. The siege was half in progress and half falling apart as the Orcs swept through their disorganised former allies coming to the dwarves aid. Desperate and at the end of their rope from decades of war, the dwarves simply had no other choice as letting the green skinned warriors join them in the defense of the dwarven halls off their mountains.

First uneasy partners in the fight for survival, fending off the frequent attacks of disheveled Velvet Black Banner troops, they later became genuine allies. In the end the dwarves would have gladly let them stay within their halls. But the Orcs felt great guilt when looking out over the barren, destroyed Landscape and the ashen city ruins of Veloren. The Orcs swore to clear the land of whatever remained of the cursed Velvet Black forces and the creatures they had brought to the continent. This hunt is what made them the hardy warrior culture they are today, charging towards any enemy that dared to cross their tribes. Only few Orcs are left today that know their history tough.

At the start of the Age of Competition most Orc tribes were fractured and without great unity.
Elves and Dwarves took more and more to the surface and the Humans reformed into greater Kingdoms and spread their territory and influence. A group of Orc Chiefs was greatly worried for the future of their kin. They still had barely any casters which put them at a great disadvantage and what culture there was, was fleeting and slowly degrading. In their plight the proud Orcs overcame their pride and went to the Brushwood Elves. Personally hearing their inquiry for help, the Queen took the group under her own tutelage to teach them knowledge that would be needed to nurture culture and find a solution to the Orcs lack in magic. After many years of studying and experimenting they found a way. For this the Elves Queen presented them with seeds from Brushwoods first and most sacred tree for the Orcs to grow into magic focus.

Thankful, the Orcs swore to stand by them should Brushwood ever be in true and grave danger. They asked for just one last favour. As the Orcs were now the tribes would never accept help from outside their kin. So, to make this help work for them, the Chiefs would spin a tale in which they stole the seeds in a daring action from the Elves holiest tree. The Queen agreed and the Orc Leaders left the Forest to unite their people under one Chief, driven by this new hope. The Rest of the Chiefs took up the role of Shamans and they grew the seeds into holy trees. From the Sap, the seeds and fallen wood of these trees they crafted talismans and strong foci and trained a new generation of orc casters. Restored to such strength, the Orcs gained new resolve and an equal place in the contest of nations. 


However, not long ago the last great Chief died during the Hunt after one of Velorens hidden evils. And foolishly she left without appointing one of her children as successor. This mistake was to split the Tribes.

When The Undead Protectorate came to life it split the tribes in two. Sworn to eradicate the darkness of The Great Malady from Veloren, one of the Chiefs children insisted that the Undead must all be destroyed, as holy duty to the Hunt. The other Child though countered, pointing out the peaceful ways the Undead had chosen to live by and the fact that they had been made against their own will. In closed, knowing company the First reminded his sibling that they were sworn to destroy all corrupted Remains of the Velvet Black. The Second reminded him that their kin only were here because they helped bring the same kind of corruption to Veloren. With the question of succession to the position of Chief unclear the tribes were split in two. Only the Shamans' refusal to take sides prevented an outbreak of violence and Sibling war. So today the Orc nation is split in two, veering close to a sibling war and their success as a nation is in peril.

# The Undead Protectorate

The Age of Competition brought development and growth. Worldly Craftsmanship and the Arcane Sciences made great leaps. Culture bloomed. Expanding Trade carried the knowledge, culture and ideas far and wide over the land.

In search for more profit a group of Aetherial Arcanists researched new ways to create a better and cheaper workforce. They first tried for the creation of golems. But the cost in magical power and physical materials was greater than what the use of Aetherial Golems as a workforce would bring in profit. Naturist Casters refused to aid the research by using Spirits of nature or even using life force to fuel such a force. 


So, in a time full of turmoil and small but numerous conflicts they went for something in between. They went for a resource that was plenty in these times. They created a workforce from the freshly deceased. It took some time, but when they presented a full workforce of undead working a field day and night in presentation to a Human Duke. The Lord was immediately fire and flame and agreed to financing the further development of this new kind of magic.

Even when the Reawakened were only able to perform simple and menial tasks their application quickly caught on with Landlords and the owners of Manufactures, Traders and organised Haulers. Early discomfort and disgust in regards to seeing the dead walk again vanished rapidly as it became clear what Profits could be made from this cheap, resilient and obedient Labour force.

The Reawakened were mostly used in Human Territories. The Dwarves respected Labour too much as a sacrosanct honor to let once dead people do actual labor and besides a few uses in hauling inventory they were never much used. The Gold Elves already had extremely cheap and (in the nobles opinion) obedient labour force so there was no need for the Undead. The Brushwood Elves just shook their heads and basically kept an unending stream of polite diplomatic messages coming that the humans may reconsider this thing. 

It took a few years until normal people of Humanity realized the consequences. The backlash from the working class when they did was unprecedented. Especially Dock workers, Farmhands and similar tasking vocations had no qualms to voice protest. The back and forth was fiery. Protests followed after protests, more than once all out riots could only be prevented by sheer luck and thanks to some calm heads on both sides. Then Human territories all but burst into flames when it came out that Owners were plundering the peoples graveyards. A revolution of the working class seemed inevitable when, suddenly, massive Orc armies stood on humanity's borders! 


It took a while until wandering Orcs reported to their Shamans about the spread of the Undead Workforce in human territory.  A group of Shamans then went to take a look at the matter themselves. They needed exactly one look upon one of the Reawakened humans to immediately recognise this as one of the many old evils their ancestors had described. Seeing that, the Great Chief wasted no time and the tribes came together and marched to the borders of human territories. 

And then, on the height of the Undead crisis, the Beloved Undead King was created. A Queen of one of Velorens most powerful Kingdoms wanted her beloved husband back. And as he stood up from the table that they revived him on, he lamented the suffering of the undead, declaring he could hear their suffering voices as they were enslaved and tormented, branding the practise as barbaric in loud protest. 


So the workers' protests escalated and in many cases closed ranks with Orc Leaders as these were openly declaring they would rid the Veloren from the Reawakened, if necessary by force. The name Undead caught on quick. The Dwarves and Brushwood Elves became increasingly queasy towards the whole matter as the technique to reawaken the Dead improved daily. And one of humanity's most beloved and now Undead King and his allies damned the practise and treatment of the Undead as barbaric, ready to strike out in defence of the Returned Souls.

A Truce was made. The Kingdoms with rioting workers and Orcs at their doorstep were just relieved to be offered a way to rid themselves of the cause for this crisis. The Undead King would take up the refugees, now named Undead. The Treaties against Slavery were expanded upon to directly recognize the Undead as individuals. Nonetheless, it stayed a crisis as the masses of Undead moved towards the Lands that united under the Name of The Undead Protectorate. Orc or otherwise motivated attacks on Convoys of the Undead were not seldom and the Protectorates Soldiers and Nobles became more aggressive in defending them as their devotion to their King took on a Cultish shade.

Most everyone was relieved when, finally, the last Undead arrived in the Protectorate. Over the generations, the contact between the lands of the Protectorate and the rest of the world got rarer. However, great advances in the medical fields became one of the protectorates greatest exports, as they learned much about the human body in the necessary work of maintaining the Undeads Bodies. They also export great amounts of ores and stone since they are not tiring when mining them and it makes sense to use this advantage to bolster their trade.


The Culture of the Undead Protectorate is a calm, sombre one, due to the fact that most of the Undead people had been forced into existence. It is a culture of great care and healing for the body and mind. Their Cities are places of friendly and respectful whispers even on the busiest days. Their numbers keep growing as no few people seek to escape death or to restore a loved one, taken too early, back to unlife. In their lands the countryside is virtually untouched since the majority of the populace by now is of the Undead and they found new ways to feed themselves rather than the wet process of digestion. They love all living things and try to protect the living just as much as themselves. Any new born child of the living populace is seen as a great reason to celebrate. Philosophy and discussions about life and death and the sense of being are important within the Protectorate. Much time is being devoted to these and other arts, since there is much time to give to these. In other words; they are great friends with the Brushwood Elves.

They prefer the nights and a dry cold, they really don’t like rain, the jungle or deserts though (since humidity and too dry conditions both are not good conditions for their bodies). Sexual attraction is not really a thing with the Undead. 

The Undead Protectorate would be a relatively peaceful place if not for some of the Orc tribes that were occasionally invading their lands and some of their own, undead citizens falling to insanity or harboring less peaceful ambitions. So The Undead Protectorate was forced to build an Army of their own. The Restless Marching are incredibly disciplined troops, never distracted by drive or swayed to cruelty by fear or anger born from fear. They have no end of stamina but that their bodies can suffer from Material Fatigue. And those devoted to protecting their land do not stop practising, ever.   

All in all The Undead Protectorate prefers peace and would like to grow their diplomacy and trade, sharing their culture and ethereal and necromantic science. But if necessary they do not shy away to send their strongest weapon, Undead Assassins. Breathless and free from bodily bother and need (does not need to eat, go for little ghuls or fear cramps from sitting still in one spot for days or weeks), there are few things that can keep an Undead Assassin or Spy from reaching their goal.      

## The Blessed Danari

By the time the nomadic Velvet Black Banner came to Veloren, the Danari had long been the bureaucratic backbone of its administration and organisation. By then, their entire culture had served the Velvet Black for so long that their home country was a distant memory.
The vision that bound their people to the Velvet Black Master was that one strong leader could unite all peoples and lead to a final and everlasting peace. 

The long, drawn out war against dwarves and elves had strained the Velvet Black’s armies and damaged that vision. Veloren’s people held steadfast and solidly over decades even as they lost battles every day. Unaccustomed to such a difficult war, their Leader became irate and unbearable, even for them. And when their master’s impatience and discontent grew into cruelty the Danari were first and center and it#s first recipients. 

It took a while, since the vision for universal peace through conquest was so integral to the Danari’s view of their place in the world. But after many secret Meetings and Council sessions behind closed curtains the Danari decided to act. 

Finally, they used the bureaucratic might they held over the Velvet Black, isolated the highest Leaders of the Black Velvet Court and their Master and struck.


The Danari not being warriors at that time, their bravest stepped and struck one of the Leaders after another. Few off these even had an inkling as Danari struck them down, but the few that were put up a desperate and angry fight. Although these fights were often short they were just as violent and many daring Danari died. 

Most of them died fighting the Master himself in the Dwarven Tower their master had chosen as a seat to oversee the conquest of Veloren from. Knowing of his powers, a good hundred Danari had taken to its servant Corridors and had exchanged places with bureaucratic personnel. They Battle was brutal and deadly. But despite the Arcane Might their Master threw against them they drove him up to the highest chambers. Dead Danari were strewn about the great Chamber at the Towers Top as one of the Danari finally managed to tackle him and grapple him over the edge. Falling over the Railing the falling Arcanist attempted to use his magics to safe himself. But the Danari he had dragged with him didn’t let loose. And while they fell the small person kept ramming his knife into the former Masters Chest. 

Under his bloody breath he started a curse but died before he even hit the ground. So only half an unfinished curse befell the Danari when him and the Danari slammed into the sides of the Dwarven Tower and then the mountain the Tower was built upon. No one knows what the curse was meant to be but as the day went on and the consequences of their Masters Death rippled through the Velvet Black Army, breaking it apart, all Danari felt it harder and harder to breath and felt drawn to bodies of water. Most likely having intended to drown them in some cruel way their Masters dying Mind had given them the ability to breath under water which banned them under the Ocean's surface. And that turned out to be their greatest Blessing.

As the Velvet Black Army fell apart the Danari organised their peoples flight to the oceans, keeping most of their recordings and culture intact as they had to move under the Ocean. As the people on Velorens now lifeless surface either had to ration their food or were starving to death, the Danari lived from the riches of the surrounding Oceans and built a solid culture. Their ability to be beyond the reach of Velorens evil protected them from cultural degradation and the same level of crisis the people on Velorens surface had been subjected to. So on the edges of the oceans their culture continued to develop and grow. Nowadays the Danair have given up the idea that it needs one strong leader and developed instead a more democratic system.  

By the age of Competition the incomplete Curse had greatly weakened. The longer a Danari spends on the surface the more they lose the curse and with that the ability to breath under water. Most Danari live under the sea levels in the Oceans and Great lakes of Veloren. But they do have many settlements on the coasts or in deep Valleys and Plains when those are not far above sea level or far from great waters. On land their bedrooms have cozy sleeping pits which are technically under sea level so the curse doesn’t wear off that fast. The average Danari regularly visit the next great bodys of water to renew the Blessing that had protected their Culture for so long. 

Though keeping parts of their long past secret, especially towards the Orcs that see in them Victims that need support, the Danari still believe in a uniting peace. But they have changed the way they want to promote this ideal. Instead of Conquest they try and peacefully spread the ideal and their democratic and culture. They even started to open up their porcelain cities by building Towers that connect to the surface and allow in visitors.  

Their views are not being well received by many of the rulers. So while they are often tolerated, their books and Philosophers often find themselves on banned lists and the Danari pressing matters can and has lead to conflict. Though they have found good friends and great philosophical Debate in the people of the Undead Protectorate.

# Veloren - Farming Differences

### Gold Elves:

In the distant past Gold Elves used magic to tend fields in the depths of the Pillar Cities. Since they started to leave the pillar cities they import most of their food. Recently though they started to bring workers to the surface to work them as farmers.

They are still rather bad at it, so the fields they have are inefficient though they keep improving. It also does not help that they treat their farm hands like prisoners.

### Brushwood Elves:

Brushwood elves do have a bit of farming, 
But most of this farming consists of small gardens for vegetables. medicines and herbs. They grow and farm a wide variety of massive, fruit bearing Trees and, opposite to common ideas of the Brushwood Elves they hunt a lot of game. They are also not shy of importing food from other lands and are currently experimenting with animal husbandry under the crowns of their large Forests.
Dwarven Unions: 
Dwarf culture functions a little like a massive united workshop. They have massive fields and farms further away from their towns and cities due to the smoke of those workshops. The dwarves are not ignorant to the negatives of their workshop culture.


During The Long Dark, subterranean mushroom farms were the main source for vegetables in the dwarven diet. Every dwarven city still has some of these, but that is more out remembrance and tradition. Turns out most things taste better when they grow in sunlight.
Human Kingdoms: 
Humans have loads of farmers that live in villages all across Veloren. They work the land and raise farm animals. Humanity’s most beloved vegetable is the amazing, beloved and versatile Potato. You can make humans VERY angry by calling Potatoes foul names.

Orc Tribes: 
The Orcs hold it with farming roughly like the elves, though they hunt much more. Orc hunting parties often coincide with their hunts for the creatures of darkness, which they sometimes also eat. Orcs can make a meal out of near anything.

The Orc tribes have fields at their homesteads, but they seldom plant anything that needs more intensive care.The old and the very young stay at home and tend these fields.
The Undead Protectorate:
The Undead have a very large number of all kinds of gardens. It’s a work that is beloved in their lands.There exists quite a sizeable minority of the living in the Protectorate, that need to be fed. Since other cultures are not that open to the Undead Protectorate importing great masses of food is out of the question. Therefore an appropriate amount of farms with crops- and herded fields dot the landscape.
Danari:
The Danari are primarily Fishermen. Fish is the main staple of their Diet and Kelp and similar greens of the ocean are their Vegetables. They trade with almost everyone for foreign food and recipes and are generally open to culinary adventures.
Veloren - The Language



To see a Timeline of the events referred to the following document. Numbers in the text point to the events on the timeline with the same number.

Veloren - Historical Timeline 


All of the Races in Veloren speak the same language just with different dialects (aside from convenience for game designers and players).
The following text details why:

In the (1.) Age Of Wonder Dwarves and Elves lived together in a peace that lasted thousands of years. Over such a long time both cultures were closely connected and their languages and scripture melded into one, just regional differences in some expressions. This is the Old Common Language.

At the end of (2.) The Great Malady, the violent and decade long invasion of the nomadic Velvet Black Banner, the Orcs freed themselves and helped the dwarves defend their Mountain Fortresses against the rests of the (4.) crumbling Armies of the Velvet Black Banner. Ashamed of their participation in the Invasion, the Orcs had no qualms to lay off the common language of the Velvet Black Banner and taking on the Dwarven dialect. The orcs stayed with the Dwarves for half a century before leaving them in a search for redemption by ridding Veloren from the foreign creatures and terrors that The Velvet Black had brought with them. Even then the now nomadic Orc tribes had regular contact with the dwarves and the new language stuck.


Nearly thousands of years later, (7.) a small of Gold Elven Workers flee the desperate conditions of the Pillar Cities, ending the age of (6.) The Long Dark. They bring the Old Common Language in word and scripture with them. The strict dictate of the Gold Elven Leaders had kept this Language in its old form for all this time. However, free of the Tyrants the Brushwood Elves use more of the Worker Slang that they otherwise only used in close quarters with Gold Elven Workers they trusted. For the most part the same Language, the Brushwood dialect allows for more warmer, lively and vulgar expressions. 

The nomadic Humans, landing on the shore of a ashen and empty Veloren, meet the Brushwood Elves and immediately connect starting (8.) The Age of Peace and Healing. For hundreds of years Brushwood Elves and Humans live and work together. The Elves teach the humans many things one of which is the elven scripture of the Common Old Language. Over time the humans take over the language of the Elves and mix it with their own expressions and dialects and important names like Potatoe.

Worried about the desolate and degenerated state of the Orc Culture and its people a (9.) Group of Orc Chiefs ask the Brushwood elves for help. The Elven Queen takes pity and teaches the Orc Leaders the tools to rebuild their culture. Over many decades the Chiefs learn and study. This refreshes the use of the Old Common Language that they had learned from the Dwarves nearly a millennium ago. As they return to their people to rebuild Orc Culture they carry this refreshment with them. Till today the Orcs speak this language. Their dialect is sharp, short and effective. The language of roaming hunters of the Darkness that always need to be ready and convey commands efficiently and in seconds.


The Gold Elves leave their Pillar Cities and immediately attack the humans settlements starting (10.) The War of Awakening. They bring the same version of the Old Common language with them that had been spoken so many years ago in the (1.) Age of Wonder.

This calls forth the Dwarves out of their own relative isolation in their mountain cities as they stormforth to aid the humans in their defence. As the brushwood Elves also come forth to support the human people the war ends with a standstill and uneasy peace. This starts (11.) The Age of Competition, the current and ongoing era of political, cultural and economic competition.

(12.) The Danari make contact with the civilisations on the coasts of Veloren. The Danari Republic has their own language. However in their motivation to share the benefits of democracy with the landlubbers of Veloren, and the Old Common Language being so present all over Veloren, every Danari child learns it as second language in school.

(14.) The Undead Industrial Revolution was a event that overwhelmingly hit the human kingdoms. So all of the Undead seeking Refuge in the Undead Protectorate are humans and speak the same language and dialects. Including the name Potato.
 

 

Made by Felixader
