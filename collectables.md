# Collectibles for Veloren 0.5 - Lore and Flavour text

## Gold Elven Artifacts

![](https://i.imgur.com/IV19SRS.png)

1. Broken Gold Elven Short Blade
This blade presumably broke in two when its wielder had to use the spine to block a series of hits. The edge is dull, but most of the metal is still in good shape despite its age. Ancient metal like this is heavily sought after by smiths, and by collectors.

![](https://i.imgur.com/LXSsDgp.png)

2. Ancient Gold Elven Belt Buckle
The metal of this belt buckle could use some polish, and it seems to have two empty sockets on its lower half. It should still be functional, though smithies will pay a good price for it.

3. Ancient Gold Elven Bronze Coin
This coin’s kind is still in use with gold elven traders. In the Gold Elven Pillar Cities, these coins are actually the only currency in use. Dwarves like the metal, but will always insist on describing its detail on paper and then smelting it down.

4. Deformed Gold Elven Shield
This ancient shield obviously has seen a lot of intense combat. It was found in an old elven ruin wedged against the wall. The shield had been slammed against the rock slabs, its corners and rim deformed and lodged into place. An entire torso and pelvis was found behind it, and the inside of said torso was caked in very old and dried blood. Whoever this elven soldier was, he had made a worthy sacrifice, judging by the spear protruding from his skeleton.

5. Petrified Gold Elven Cake
This kind of elven cake is moreso a weapon than a confectionery. Long ago, elves liked to form everything into fancy cake shapes, even if it was not a cake. This occasionally drives human patissieres mad. The cake is so old that its only use is to bash someone's head in.

6. Worn Elven Woman Figure
Old writing attests gold elven society's attitude of equality between men and women. It is a surprise that later historic evidence from ancient battlefields depicts a change in said equality. Armor and skeletal evidence shows a relatively equal share of female and male remains on the battlefields, but as the battlefields get younger by decade, the remains are more and more of male fighters, many of whom carry carvings of their partner. Something must have changed to have moved the roles of women and men into the roles of today.

7. Grated Down Gold Elven Whetstone.
This whetstone has seen a lot of use, often in haste as the irregular markings on its rim shows. Any experienced soldier, warrior, or mercenary, will look at this and see that someone had to sharpen their blades a lot, presumably during restless nights at war.

## Dwarven Artifacts

1. Broken Dwarven Union Warhammer
Dwarves don’t stop fighting, even when cornered. This heavily scuffed warhammer gave in before its wielder did. The signet of the Dwarven Union Army still shines proudly from its flank.


2. Ancient Dwarven Union Bolt Bag
This bag of bolts is old. Its leather is dry and broken, the strap is ripped apart and frayed and the buckle is missing. The bolts are feathered with thin wood slices. This testifies to desperate times and measures to make ammunition for dwarven crossbows.

3. Ancient Dwarven Union Soldiers Field Cutlery
A marching soldier must eat. “No fight without food”, as the saying goes. Like all Dwarven cutlery, this one is in pristine condition. The drill to clean the cutlery was thought to be extremely strict, second only to the method of cleaning itself. An army eating dirty is a dead army.

4. Ancient Dwarven Union Whetstone
This whetstone, found in the wild between roots, is a pitiful sight. Dwarven Army whetstones are given upon a soldier's first march into real battle. Dwarven whetstones typically hold out for a long time, even with the tradition for a soldier to clean their blade upon scuffing. This one was found in the midst of an ancient battlefield, with nary a scratch on its surface.

5. Dwarven Union Bacon
Every Dwarf carries this ration in the likely event they are needed before a meal can be served. Nutritious and easy to pack, it can keep a soldier afloat for a while. These two slices of bacon are so old they are petrified.Tragically, many of these can be found in the old ruins of Fortresses. Not so tragically, however, they serve as a deliciously smelling fire starter.

6. Worn Dwarven Family Figure Set
A figure set representing members of the most intimate family unit in Dwarven society, the Inner Family. They are often found with the remains of Dwarven soldiers felled long ago in battle. The bottom of these figures often carry the family name, name of the individual, and the date of birth. Today's Dwarven Union will pay good money for the excavation of figures. A figure of this particular set, one of the children, seems to also carry a death date, with a blade besides it.

7. Dwarven Union Weapon Handle with Petrified Hand
This handle was found with a dwarven hand still gripping on. The handle ends seem caked in dried blood, so it is safe to assume not having a blade did not stop this fierce soldier.

# Human Artifacts 


1. Ancient Human Seedbag
These seedbags are often given within a family from generation to generation. They are usually treated with reverence, even in noble families that are long beyond the toil of human farmers. The bags are often made from patches of leather, trading hands over decades. Rituals like branding names and branding family sigils differ between regions, and sometimes between individual families. The respect and reverence towards this tradition, however, seems to be universal in human culture.

2. Ancient Human Depiction of Potato Soup
An ancient recipe for potato soup. There are as many variations to this dish as there are grandmothers that pass it on to their daughters and granddaughters.

3. Picture of a Harvest
A nice, albeit bleached painting of the Fall Harvest, fraying at the seams. It looks like the canvas was already in this condition, seeing as there is a layer of paint over the frays. On it is written a message of gratitude; “For my dear friend. Thank you for helping me through the winter.”

4. Broken Pikeman Helmet
The faceplate to this helmet seems to be broken. Whatever hit it slid off to the left, denting the side. There seems to be no blood on the helmet anywhere, indicating the wearer was likely not killed.

5. Old Horror Story
Some of the most prevailing tales handed down by oral tradition in human culture would be creepy tales from the Ash Famine. While the stories vary between regions and cultures, they are all creepy in the same ways. They were kept alive by word of mouth, causing this book to be published. It raised interest to both scholars and hobbyist anthropologists.

6. Portrait of a Human At Arms
This portrait depicts the tools, weapons and armor of a human man at arms in detail, with pompous description. Similar pictures can be found all over the human kingdoms. At first produced as propaganda in one kingdom, its great success prompted imitation in every kingdom. Boys love to collect these and imitate what is depicted.

7. Old Nobleman’s Ring
This is the ring of a nobleman. There's not much to say on the appearance itself. However, the ring was found in a wolf's stomach with a fingerbone still in it. Before that, it was taken off a tax collector who was killed by bandits in a raid on the man's caravan. The location of the original owner is unknown. Perhaps you shouldn't hold onto this ring for too long.

## Orc Artifacts

1. Dried Monster Claw In Necklace
It is not clear what creature this claw originated from. It is likely to either be a foe to the owner, or the owner’s first hunt.The question remains as to what happened to the owner of this necklace.

2. Ancient Orcish Fabric
This appears to be a memento of some kind. Few know of their origin, and fewer even possess one. Most of these pieces of fabric aren’t worn by their owners, rather kept safe in a concealed place. However this piece came to be in your hands, try to avoid showing it to orcs you do not trust. 

3. Ancient Orcish Cornbread 
Yes, it is still good. No, it is not really made from corn. This is very popular with traveling folks since it is incredibly nutritious, even after all this time. Orcs call it cornbread due to the contents being so stomach churning, people would not consume it if they learned the truth.

4. Orcish Bag of Spears
Orcs are specialists in the hunt, their spears a key factor to it. Due to the hassle of constantly remaking spears, the orcs developed a “breakaway tip”, allowing them to replace their blades, without entirely throwing out the spear. This was later refined, and different tips were devised. 

5. Orcish Neck Axe
This thing is horrifically effective. It is used to lop limbs from creatures and, should the chance arise, prematurely end a fight via decapitation.

6. Depiction of Orcish Courting Rituals
If a couple comes together and wants to unite in an official, romantic bond, they undergo a series of ritualistic competitions. Through these competitions, they can find their role within the marriage. If there are two particularly competitive partners, the contests can get quite fierce. In quite a few cases, this can cause a couple to reconsider their bond.

7. Depiction of Orc Oath
All orcs swear to hunt and fight the darkness and its creatures, until the day they give their last breath.. The oaths differ between tribes and are kept secret from most, save for other orcs and trusted members. But this image of two orcs fending off the darkness can be found in many orc settlements or camping places within orcish lands. The top of the disk has been dunked into tar and polished, symbolising the offending darkness and its defeat.

## Undead Artifacts

1. Necklace of the Restless Marching Army
The Undead Protectorate is a peaceful and individualistic people. However, they are not ignorant pacifists towards danger from outside their culture. The Restless Marching are the Protectorate’s army. They are undead soldiers dedicated to military training, combat, and tactics, constantly honing their abilities and knowledge. Should someone ever be foolish enough to mistake peace with pacifism and attack the undead, the Restless Marching will make them regret the rest of their living days.

2. Glass Vial Attached To Necklace
A memento of an undead individual, reminding them of their reawakening into slavery, and the subsequent struggle for freedom.


3. Symbolic Carving of the Undead Royal Couple
The queen, stricken by the death of her husband and soulmate, called the new necromantic experts to revive him. The awakened king felt great pity for his fellow undead, and founded the Undead Protectorate. The fugitives of the undead slave riots joined his kingdom, growing his citizenry. With time, the queen and her living subjects followed the king, and new citizens helped grow the kingdom to its current size.

4. Glass Vial with Blond Hair.
The memento of an undead individual, containing a few blond locks. What it might represent is not obvious. 

5. Undead Protectorate Surgery Kit
The undead were forced to learn the care and maintenance of their bodies. This made them experts in anatomy, especially human anatomy. They are open and willing to share their knowledge for a fair price. These surgical tools, containing scissors and scalpel, are central for said maintenance.


6. Split Silver Hand Mirror 
While most undead civilians enjoy a good life, the trauma of their reawakening left a scar on most of the first generation. The healing of the mind is a central topic within the people of the Undead Protectorate, due to the pain of the past showing its face from time to time. This broken hand mirror is the result of such a painful moment.

7. Framed Undead Revolutionary Bandage
“They awoke us from eternal slumber, so that we would toil endlessly for their gain. We spoke up in protest, and were denied our voice. So we shouted to be heard, and fought for our freedom, from shackles and manufacture, from the fields to the mines.”

## Danari Artifact

1. Danari Republic Coin 
This coin carries the halfrings of the Danari Republic, symbolising the democratic elected council. Their coin is made of copper, and coated in an alloy that prevents it from immediately rusting in rivers or sea water.

2. Danari Pamphlet
A pamphlet of danari origin that explains the power of the people. These sorts of papers are not well liked by authorities in the above water kingdoms. Their appearance outside of danari lands have caused diplomatic ill-will in the past.

3. Danari Diver’s Mask
The Danari weren’t made for life underwater. The curse that forced them to do so changed them in some ways, like being able to filter air from the water. However, the danari do not have second eyelids or nictitating membranes to protect their eyes from the water, leading to the invention of their diver mask.

4. Mosaic of Danari Subaquatic Cities
A mosaic depicting a danari domed city. At the start of their banishment, the danari were reliant on underwater caves or cracks in the ocean floor. They have come far from those humble ramshackle beginnings to today’s domed cities.

5. Image of Danari Seafood Dish
A delicious seafood platter with fish, crab, and a mix of kelp, used in eateries. Spacy eateries such as the advertisement for this one exist everywhere, where a great number of workers and other people need to be fed in a short time. None of the other cultures' eateries can reach the quality of food that are served in danari eateries.

6. Danari Republic Fish Riders Emblem
The mark of the Danari Fish Riders unit. The name and symbols are a bit misleading, since the animals they ride are mammals, and the saddles used are not as convoluted as pictured. The Fish Riders are feared enemies for any fool owning great fleets of ships. Entire fleets have sunken silently and overnight, through holes bored into their hulls from below.

7. Mysterious Dagger And Document
If someone were to see a danari wear this sigil close by, and it seems on purpose, they may deem themselves protected, as the danari do not tend to threaten outsiders. If it seems like an accident, it is assumed they have angered the danari in some way.
